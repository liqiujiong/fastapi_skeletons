from __future__ import absolute_import, unicode_literals

import logging

from apps.ext.wechatpy.client import WeChatClient  # NOQA
from apps.ext.wechatpy.component import ComponentOAuth, WeChatComponent  # NOQA
from apps.ext.wechatpy.exceptions import WeChatClientException, WeChatException, WeChatOAuthException, WeChatPayException  # NOQA
from apps.ext.wechatpy.oauth import WeChatOAuth  # NOQA
from apps.ext.wechatpy.parser import parse_message  # NOQA
from apps.ext.wechatpy.pay import WeChatPay  # NOQA
from apps.ext.wechatpy.replies import create_reply  # NOQA

__version__ = '1.8.2'
__author__ = 'messense'

# Set default logging handler to avoid "No handler found" warnings.
try:  # Python 2.7+
    from logging import NullHandler
except ImportError:
    class NullHandler(logging.Handler):
        def emit(self, record):
            pass

logging.getLogger(__name__).addHandler(NullHandler())
