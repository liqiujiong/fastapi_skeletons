#!/usr/bin/evn python
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   文件名称 :     insert
   文件功能描述 :   功能描述
   创建人 :       小钟同学
   创建时间 :          2021/7/12
-------------------------------------------------
   修改描述-2021/7/12:         
-------------------------------------------------
"""
from dataclasses import dataclass
from apps.ext.ormx.models.base import Base
from apps.ext.ormx.exceptions import ClientConfigBadException


@dataclass
class Delect(Base):
    pass

    def __columnize(self, columns):
        # 把字典类型的资源化为元祖类型，且元祖的内容元素使用“” 进行包裹处理！
        return tuple(columns).__str__().replace('\'', '"')

    def __valueize(self, data):
        print("data", data)
        # 把字典类型的值进行值使用（，）的方式进行包裹处理，取字典的原始的值，
        # 首先先把所有的值给添加到一个元祖列表里面
        # 然后列表进行串联
        return ','.join([tuple(index.values()).__str__() for index in data])

    def delete(self):
        self.__sql__ = self._compile_delete()
        return self

    def _compile_delete(self):
        if not self.select_table:
            raise ClientConfigBadException(errmsg='请设置需要查询查询的表')
        return 'delete from {}{}{}'.format(self.select_table, self._compile_where(), self._compile_returning())
