#!/usr/bin/evn python
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   文件名称 :     __init__.py
   文件功能描述 :   功能描述
   创建人 :       小钟同学
   创建时间 :          2021/7/28
-------------------------------------------------
   修改描述-2021/7/28:         
-------------------------------------------------
"""
import abc
from typing import Tuple


class Backend:
    @abc.abstractmethod
    async def get_with_ttl(self, key: str) -> Tuple[int, str]:
        raise NotImplementedError

    @abc.abstractmethod
    async def get(self, key: str) -> str:
        raise NotImplementedError

    @abc.abstractmethod
    async def set(self, key: str, value: str, expire: int = None):
        raise NotImplementedError

    @abc.abstractmethod
    async def clear(self, namespace: str = None, key: str = None) -> int:
        raise NotImplementedError