#!/usr/bin/evn python
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   文件名称 :     __init__.py
   文件功能描述 :   功能描述
   创建人 :       小钟同学
   创建时间 :          2021/9/23
-------------------------------------------------
   修改描述-2021/9/23:         
-------------------------------------------------
"""
from fastapi import APIRouter
from apps.ext.logger.contexr_logger_route import ContextLogerRoute
bp = APIRouter(tags=['斡旋中医馆线上预约系统V1'],prefix='/hs/api/v1',route_class=ContextLogerRoute)

