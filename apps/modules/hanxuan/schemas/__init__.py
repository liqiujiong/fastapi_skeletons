#!/usr/bin/evn python
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   文件名称 :     __init__.py
   文件功能描述 :   功能描述
   创建人 :       小钟同学
   创建时间 :          2021/9/24
-------------------------------------------------
   修改描述-2021/9/24:         
-------------------------------------------------
"""
from pydantic import BaseModel, Field
from fastapi import Depends, Query
from typing import Optional
from fastapi import Depends
from apps.modules.hanxuan.dependencies.ip import get_client_ip
from pydantic import BaseModel, ValidationError, validator
from apps.response.json_response import Fail

class DoctorInfoForm(BaseModel):
    # 定义的是查询参数的值
    # 模糊查询会传递的此参数
    dno: str = Query(...,min_length=1,description="医生编号ID")


class DoctorTiempmCheckForm(DoctorInfoForm):
    # 新增需要时段索引排班索引编号
    nsindex: str = Query(...,min_length=1,description="医生排版时段序号索引编号ID")



class SubscribeOrderForm(BaseModel):
    '''
    下单需要相关字段信息
    '''
    dno: str = Query(..., min_length=1,description="医生编号ID")
    nsindex: str = Query(..., min_length=1,description="医生排版时段序号索引编号ID")


class WxCodeForm(BaseModel):
    '''
    下单需要相关字段信息
    '''
    code: str = Query(..., min_length=1,description="微信CODE")


class TopaySubscribeOrderForm(SubscribeOrderForm):
    '''
    下单需要相关字段信息
    '''
    payactions:  str = Query(1, min_length=1,description="支付类型（1:微信 2：支付宝）")
    visit_uname: str = Query(..., min_length=2,description="订单所属-就诊人姓名")
    visit_uopenid: str = Query(..., min_length=1, description="订单所属-就诊人微信授权ID")
    visit_uphone: str = Query(..., min_length=11, description="订单所属-就诊人联系电话")
    visit_usex: str = Query(..., min_length=1, description="订单所属-就诊人性别")
    visit_uage: int = Query(...,gt=1,description="订单所属-就诊人年龄")
    # 客户端的IP地址----------------通过依赖的方式进行检测校验
    client_ip: str  = Query(Depends(get_client_ip), description="订单所属-就诊人性别")

    # @validator('username')
    # def username_alphanumeric(cls, v):
    #     assert v.isalpha(), 'must be alphanumeric'
    #     return v


class SubscribeOrderCheckForm(BaseModel):
    # 新增需要时段索引排班索引编号
    dno: str = Query(...,min_length=1,description="医生编号ID")
    orderid: str = Query(..., min_length=1, description="订单编号ID")
    visit_uphone: str = Query(..., min_length=1, description="就诊人电话号码")
    visit_uopenid: str = Query(..., min_length=1, description="就诊人微信ID")


class UserOrderIonfoListForm(BaseModel):
    '''
    下单需要相关字段信息
    '''
    visit_uopenid: str = Query(..., min_length=1,description="就诊人微信ID")