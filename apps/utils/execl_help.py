#!/usr/bin/evn python
# coding=utf-8
# + + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + +
#        ┏┓　　　┏┓+ +
# 　　　┏┛┻━━━┛┻┓ + +
# 　　　┃　　　　　　 ┃ 　
# 　　　┃　　　━　　　┃ ++ + + +
# 　　 ████━████ ┃+
# 　　　┃　　　　　　 ┃ +
# 　　　┃　　　┻　　　┃
# 　　　┃　　　　　　 ┃ + +
# 　　　┗━┓　　　┏━┛
# 　　　　　┃　　　┃　　　　　　　　　　　
# 　　　　　┃　　　┃ + + + +
# 　　　　　┃　　　┃　　　　Codes are far away from bugs with the animal protecting　　　
# 　　　　　┃　　　┃ + 　　　　神兽保佑,代码无bug　　
# 　　　　　┃　　　┃
# 　　　　　┃　　　┃　　+　　　　　　　　　
# 　　　　　┃　 　　┗━━━┓ + +
# 　　　　　┃ 　　　　　　　┣┓
# 　　　　　┃ 　　　　　　　┏┛
# 　　　　　┗┓┓┏━┳┓┏┛ + + + +
# 　　　　　　┃┫┫　┃┫┫
# 　　　　　　┗┻┛　┗┻┛+ + + +
# + + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + +"""
"""
# 版权说明
# + + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + +

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
# + + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + +

# @Time  : 2020/3/18 16:28

# @Author : mayn

# @Project : ZBotte

# @FileName: execl_help.py

# @Software: PyCharm

# 作者：小钟同学

# 著作权归作者所有

# 文件功能描述: 
"""

import numbers

from openpyxl import Workbook, load_workbook


class ExeclProcesser():
    def __init__(self):
        pass

    def load_execl(self, filepath):
        '''
        载入execl
        '''
        self.wb = load_workbook(filepath)

    def load_sheet(self, sheetSign):
        '''
        载入sheet
        sheetSign: 如果为int，则是sheet的编号；如果为str，则是sheet的名称
        '''
        if isinstance(sheetSign, numbers.Integral):
            self.sheet = self.wb.get_sheet_by_name(
                self.wb.get_sheet_names()[sheetSign]
            )
        elif isinstance(sheetSign, str):
            self.sheet = self.wb.get_sheet_by_name(sheetSign)

    def new_execl(self, sheetSign, index=0):
        '''
        新建execl
        '''
        self.wb = Workbook()
        self.wb.create_sheet(sheetSign, index=index)
        self.load_sheet(index)

    def new_sheet(self, sheetSign, index=0):
        '''
        新建工作表
        '''
        self.wb.create_sheet(sheetSign, index=index)

    def save_execl(self, filepath):
        self.wb.save(filepath)

    def get_size(self):
        '''
        返回最大行列 (最大列，最大行)
        '''
        return (self.sheet.max_column, self.sheet.max_row)

    def load_iterdata(self, type='rows', filter=True):
        '''
        按行/列的数据生成器
        'rows' / 'columns'
        '''
        dataiter = self.sheet.rows if type == 'rows' else self.sheet.columns
        max_len = self.sheet.max_row if type == 'rows' else self.sheet.max_column
        haveFilter = False
        for celldata in dataiter:
            if filter and not haveFilter:
                haveFilter = True
                continue
            else:
                yield [cell.value for cell in celldata[0:max_len]]

    def get_cell(self, row, column):
        '''
        获取cell
        '''
        return self.sheet.cell(row=row - 1, column=column - 1)

    def get_linedata(self, index, type='rows'):
        '''
        获取某一行数据
        '''
        dataiter = self.sheet.rows if type == 'rows' else self.sheet.columns
        max_len = self.sheet.max_row if type == 'rows' else self.sheet.max_column
        return [
            cell.value for cell in list(dataiter)[index][0:max_len]
            ]

    def write_line(self, linedata, type='rows'):
        self.sheet.append(linedata)