#!/usr/bin/evn python
# coding=utf-8
# + + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + +
#        ┏┓　　　┏┓+ +
# 　　　┏┛┻━━━┛┻┓ + +
# 　　　┃　　　　　　 ┃ 　
# 　　　┃　　　━　　　┃ ++ + + +
# 　　 ████━████ ┃+
# 　　　┃　　　　　　 ┃ +
# 　　　┃　　　┻　　　┃
# 　　　┃　　　　　　 ┃ + +
# 　　　┗━┓　　　┏━┛
# 　　　　　┃　　　┃　　　　　　　　　　　
# 　　　　　┃　　　┃ + + + +
# 　　　　　┃　　　┃　　　　Codes are far away from bugs with the animal protecting　　　
# 　　　　　┃　　　┃ + 　　　　神兽保佑,代码无bug　　
# 　　　　　┃　　　┃
# 　　　　　┃　　　┃　　+　　　　　　　　　
# 　　　　　┃　 　　┗━━━┓ + +
# 　　　　　┃ 　　　　　　　┣┓
# 　　　　　┃ 　　　　　　　┏┛
# 　　　　　┗┓┓┏━┳┓┏┛ + + + +
# 　　　　　　┃┫┫　┃┫┫
# 　　　　　　┗┻┛　┗┻┛+ + + +
# + + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + +"""
"""
Author = zyx
@Create_Time: 2018/6/28 14:12
@version: v1.0.0
@Contact: 308711822@qq.com
@File: weight_prize_helper.py
@文件功能描述:
"""
import bisect
import random

#按权重比例返回中奖的概率
class WeightRandom:
    def __init__(self, items):
        weights = [w for _, w in items]
        self.goods = [x for x, _ in items]
        self.total = sum(weights)
        self.acc = list(self.accumulate(weights))

    def accumulate(self, weights):  #
        cur = 0
        for w in weights:
            cur = cur + w
            yield cur

    def __call__(self):
        return self.goods[bisect.bisect_right(self.acc, random.uniform(0, self.total))]


# 按权重分配返回用户中奖信息列表
# 1=》苹果电脑，2=》5块钱话费，3=》电影票，4=》京东卷，其他值=》谢谢参与
def get_prizes_indata():
    # wr = _WeightRandom([('5元话费', 100000000000), ('电影票2张', 100), ('苹果电脑', 1), ('500元京东购物卡', 50)])
    wr = WeightRandom([('5元话费#2', 2), ('谢谢参与#0', 10000), ('5元话费#2', 5)])
    gifs = wr().split('#')
    data = {'gifs_content': gifs[0], 'gifs_index': gifs[1]}
    return data

# 示例
# for i in range(0,20000):
#     print(get_prizes_indata())